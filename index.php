<!DOCTYPE html>
<html>
	<head>
		<title>Upload de fichier</title>
		<link rel="stylesheet" href="api/css/bootstrap.min.css"/>
		<meta charset="UTF-8"/>
	</head>

	<body>
		<br/>
		<br/>
		<div class = "container" align = "center">
			<h2>Upload File by AJAX</h2>
			<br/>
			<br/>
			<label>Select Image</label>
			<input type="file" name="file" id = "file" />
			<br/>
			<span id = "uploaded_image"></span>

		</div>

		
		<script type="text/javascript" src = "api/js/jquery.min.js"></script>
		<script type="text/javascript" src = "api/js/bootstrap.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				//console.log('hello !');

				$(document).on('change', '#file', function(){
					var property = document.getElementById('file').files[0];
					var image_name = property.name;
					var image_extension = image_name.split('.').pop().toLowerCase(); 

					if (jQuery.inArray(image_extension, ['gif', 'png', 'jpg', 'jpeg']) == -1 ) {

						alert('Invalid Image File');
					}

					var image_size = property.size;

					if ( image_size > 2000000 ) {
						alert('Image File Size is very big');
					}
					else {
						var form_data = new FormData();
						
						form_data.append("file", property );
						$.ajax({
							url:"upload.php",
							method:"POST",
							data:form_data,
							contentType:false,
							cache:false,
							processData:false,
							beforeSend:function(){
								$('#uploaded_image').html("<label class='text-success'>Image Uploading...</label>");
							},
							success:function(data){
								$('#uploaded_image').html(data);
							}
						});
						/*$.post ( 'Upload.php', form_data, function ( data1 ) {
								alert ( data1.res );
						}, "json");*/

					}
				});
			});
		</script>
	</body>
</html>